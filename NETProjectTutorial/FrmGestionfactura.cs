﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionfactura : Form
    {
        private DataSet dsGestionFactura;
        private BindingSource bsGestionFactura;

        public DataSet DsGestionFactura
        {
            get
            {
                return dsGestionFactura;
            }

            set
            {
                dsGestionFactura = value;
            }
        }

        public FrmGestionfactura()
        {
            InitializeComponent();
            bsGestionFactura = new BindingSource();
        }



        private void btnVer_Click(object sender, EventArgs e)
        {

        }

        private void cmbEmpleados_SelectedIndexChanged(object sender, EventArgs e)
        {
            busqueda();
        }
        private void busqueda() {
            dsGestionFactura.Tables["GestionFactura"].Rows.Clear();
            int i = 0;
            foreach (DataRow dr in dsGestionFactura.Tables["Factura"].Rows)
            {
                if (dr["Empleado"].ToString().Equals(cmbEmpleados.SelectedValue.ToString()))
                {
                    DataRow drGFactura = dsGestionFactura.Tables["GestionFactura"].NewRow();

                    drGFactura["Codigo"] = dsGestionFactura.Tables["Factura"].Rows[i]["CodFactura"];
                    drGFactura["Fecha"] = dsGestionFactura.Tables["Factura"].Rows[i]["Fecha"];
                    drGFactura["Subtotal"] = dsGestionFactura.Tables["Factura"].Rows[i]["SubTotal"];
                    drGFactura["Iva"] = dsGestionFactura.Tables["Factura"].Rows[i]["Iva"]; ;
                    drGFactura["Total"] = dsGestionFactura.Tables["Factura"].Rows[i]["Total"]; ;

                    dsGestionFactura.Tables["GestionFactura"].Rows.Add(drGFactura);
                }
                i++;
            }
            dgvGestionFactura.AutoGenerateColumns = true;
        }

        private void FrmGestionfactura_Load(object sender, EventArgs e)
        {
            cmbEmpleados.DataSource = dsGestionFactura.Tables["Empleado"];
            cmbEmpleados.DisplayMember = "NA";
            cmbEmpleados.ValueMember = "Id";

            bsGestionFactura.DataSource = DsGestionFactura;
            bsGestionFactura.DataMember = DsGestionFactura.Tables["GestionFactura"].TableName;
            dgvGestionFactura.DataSource = bsGestionFactura;
            busqueda();
        }
    }
}
